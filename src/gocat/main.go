package main

import (
	"fmt"
	"io"
	"os"
)

const BUFFERSIZE = 1024

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: gocat [file ...]")
	}
	files := os.Args[1:]
	for _, file := range files {
		if err := processFile(file); err != nil {
			fmt.Fprintf(os.Stderr, "gocat: %v", err)
		}
	}
}

func processFile(filename string) error {
	var f *os.File
	if filename == "-" {
		f = os.Stdin
	} else {
		var err error
		// f, err := doesn't work as this would keep the f scope local
		f, err = os.Open(filename)
		if err != nil {
			return err
		}
		defer f.Close()
	}
	buf := make([]byte, BUFFERSIZE)
	if _, err := io.CopyBuffer(os.Stdout, f, buf); err != nil {
		return err
	}
	return nil
}

// alternative implementation (if not using io.CopyBuffer)
func copyBuffer(destination io.Writer, source io.Reader, buf []byte) error {
	for {
		n, err := source.Read(buf)
		if err != nil && err != io.EOF {
			return err
		}
		if n == 0 {
			break
		}

		if _, err := destination.Write(buf[:n]); err != nil {
			return err
		}
	}
	return nil
}
