package main

import (
	"fmt"
	"sync"
)

const (
	MESSAGES  = 6
	PRODUCERS = 2
	CONSUMERS = 4
)

var (
	done        chan struct{}
	ch          chan string
	wgProducers sync.WaitGroup
	wgConsumers sync.WaitGroup
)

func main() {
	ch = make(chan string)
	done = make(chan struct{})
	for i := 1; i <= PRODUCERS; i++ {
		if i != PRODUCERS {
			createProducer(i, MESSAGES/PRODUCERS)
		} else {
			createProducer(i, MESSAGES/PRODUCERS+MESSAGES%PRODUCERS)
		}
	}
	for i := 1; i <= CONSUMERS; i++ {
		createConsumer(i)
	}
	// wait for producers
	wgProducers.Wait()
	close(done)
	// wait for consumers
	wgConsumers.Wait()
	fmt.Println("Consumers and producers are finished.")
}

func createConsumer(i int) {
	fmt.Printf("Starting consumer %d\n", i)
	wgConsumers.Add(1)
	go func() {
		defer wgConsumers.Done()
		for {
			select {
			case <-done:
				fmt.Printf("Consumer %d done\n", i)
				return
			case msg := <-ch:
				fmt.Printf("Consumer %d received message: %v\n", i, msg)
			}
		}
	}()
}

func createProducer(i, messages int) {
	fmt.Printf("Starting producer %d\n", i)
	wgProducers.Add(1)
	go func() {
		defer wgProducers.Done()
		for m := 1; m <= messages; m++ {
			ch <- fmt.Sprintf("Msg %d from producer %d", m, i)
		}
		fmt.Printf("Producer %d done\n", i)
	}()
}
