package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

const (
	url = "https://api.chucknorris.io/jokes/random"
)

func main() {
	fmt.Printf("Calling joke service %s ...\n", url)
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("Can't access %s\n#%v", url, err)
	}
	jokeResponse := JokeResponse{}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&jokeResponse)
	if err != nil {
		log.Fatalf("Bad response\n:#%v", err)
	}
	log.Printf("Joke of the Day:\n%s", jokeResponse.Value)
}
