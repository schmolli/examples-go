package main

type JokeResponse struct {
	Id         string   `json:"id"`
	Value      string   `json:"value"`
	Categories []string `json:"categories"`
}
