## GoK8S

1. Ensure that Docker Desktop is running with Kubernetes enabled
   
2. Build the docker image

        # docker build . -t turngeek/gok8s

3. Run the pod on Kubernetes

        # kubectl apply -f gok8s.yaml

4. Run the pod on Kubernetes
