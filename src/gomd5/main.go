package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		//If no file names are given, read from stdin
		//Use CRTL-D to signal eof.
		hash, err := processInput(os.Stdin)
		if err != nil {
			fmt.Fprintf(os.Stderr, "gomd5: %v", err)
		} else {
			fmt.Printf("%x\n", hash)
		}
	} else {
		files := os.Args[1:]
		for _, file := range files {
			f, err := os.Open(file)
			if err != nil {
				fmt.Fprintf(os.Stderr, "gomd5: %v", err)
			}
			hash, err := processInput(f)
			if err != nil {
				fmt.Fprintf(os.Stderr, "gomd5: %v", err)
			}
			f.Close()
			fmt.Printf("%x\n", hash)
		}
	}
}

func processInput(input io.Reader) (string, error) {
	h := md5.New()
	if _, err := io.Copy(h, input); err != nil {
		return "", err
	}
	return string(h.Sum(nil)), nil
}
