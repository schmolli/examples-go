package main

import (
	"fmt"
	"os"
	"strings"
	"unicode"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: gopalindrome [sentence]")
	} else {
		sentence := strings.Join(os.Args[1:], " ")
		var result string
		if isPalindrome := isPalindrome(sentence); isPalindrome {
			result = "a"
		} else {
			result = "not a"
		}
		fmt.Printf("Your sentence: '%s' is %s palindrome!\n", sentence, result)
	}
}

func isPalindrome(s string) bool {
	var letters []rune
	for _, r := range s {
		if unicode.IsLetter(r) {
			letters = append(letters, unicode.ToLower(r))
		}
	}
	for i := range letters {
		if letters[i] != letters[len(letters)-1-i] {
			return false
		}
	}
	return true
}
