package main

import (
	"testing"
)

func TestIsPalindrome(t *testing.T) {
	tests := []struct {
		str      string
		expected bool
	}{
		{
			"Reit nie, ein Tier",
			true,
		},
		{
			"Kein Palidrom",
			false,
		},
		{
			"Abba",
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.str, func(t *testing.T) {
			isPalindrome := isPalindrome(tt.str)
			if isPalindrome != tt.expected {
				t.Errorf(`IsPalindrome("%s") returns %v but must be %v`, tt.str, isPalindrome, tt.expected)
			}
		})
	}
}
