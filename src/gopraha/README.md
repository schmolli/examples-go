# gopraha

Producer / Consumer example using Kafka. 

## Run the example

To run the example, first start the Kafka using docker:

    # docker compose up

Test starts up Kafka and Zookeeper. Check if a client can connect to the Kafka server:

    # nc -z localhost 29092 

If so, you can run the go example:

    # go mod download
    # go run .

To shut down the Kafka server call:

    # docker compose down
