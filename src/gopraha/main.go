package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"

	"github.com/segmentio/kafka-go"
)

const (
	MESSAGES  = 10
	PRODUCERS = 3
	CONSUMERS = 5

	KAFKA_HOST = "localhost:29092"
	TOPIC      = "gopraha"
)

var (
	wg sync.WaitGroup
)

func main() {
	ensureTopic(TOPIC, CONSUMERS)
	ctx := createCancelContext()
	for i := 1; i <= PRODUCERS; i++ {
		if i != PRODUCERS {
			createProducer(ctx, TOPIC, i, MESSAGES/PRODUCERS)
		} else {
			createProducer(ctx, TOPIC, i, MESSAGES/PRODUCERS+MESSAGES%PRODUCERS)
		}
	}
	for i := 1; i <= CONSUMERS; i++ {
		createConsumer(ctx, TOPIC, i)
	}

	// wait for producer and consumers
	wg.Wait()
	log.Println("Consumers and producers are finished.")
}

func createCancelContext() context.Context {
	ctx, cancel := context.WithCancel(context.Background())
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	go func() {
		select {
		case <-c:
			log.Println("Signal catched. Calling cancel.")
			cancel()
		case <-ctx.Done():
		}
		signal.Stop(c)
		log.Println("Signal watcher successfully removed.")
	}()
	return ctx
}

func createConsumer(ctx context.Context, topic string, i int) {
	log.Printf("Starting consumer %d\n", i)
	wg.Add(1)
	go func() {
		defer wg.Done()
		r := kafka.NewReader(kafka.ReaderConfig{
			Brokers:  []string{KAFKA_HOST},
			GroupID:  "consumer-group-id",
			Topic:    topic,
			MinBytes: 10e3, // 10KB
			MaxBytes: 10e6, // 10MB
		})
		for {
			msg, err := r.ReadMessage(ctx)
			if err != nil {
				log.Printf("Consumer %d failed to read message: %v\n", i, err)
				break
			}
			log.Printf("Consumer %d received message at topic/partition/offset %v/%v/%v: %s = %s\n", i, msg.Topic, msg.Partition, msg.Offset, string(msg.Key), string(msg.Value))
		}
		if err := r.Close(); err != nil {
			log.Printf("Consumer %d could not be closed: %v\n", i, err)
		}
		log.Printf("Consumer %d successfully closed\n", i)
	}()
}

func createProducer(ctx context.Context, topic string, i, nr int) {
	log.Printf("Starting producer %d\n", i)
	wg.Add(1)
	go func() {
		defer wg.Done()
		w := &kafka.Writer{
			Addr:     kafka.TCP(KAFKA_HOST),
			Topic:    topic,
			Balancer: &kafka.LeastBytes{},
		}
		var messages []kafka.Message
		for m := 1; m <= nr; m++ {
			messages = append(messages, kafka.Message{
				Value: []byte(fmt.Sprintf("Msg %d from producer %d", m, i)),
			})
		}
		if err := w.WriteMessages(ctx, messages...); err != nil {
			log.Printf("Producer %d failed to write nr: %v\n", i, err)
		}
		log.Printf("Producer %d successfully sent messages\n", i)
		if err := w.Close(); err != nil {
			log.Printf("Producer %d could not be closed: %v\n", i, err)
		}
		log.Printf("Producer %d successfully closed\n", i)
	}()
}
