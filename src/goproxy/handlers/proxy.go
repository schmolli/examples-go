package handlers

import (
	"io"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func Proxy(upstream string) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		url := upstream + vars["req"]
		log.Printf("Calling upstream url: %s\n", url)
		resp, err := http.Get(url)
		if err != nil {
			handleError(w, url, err)
			return
		}
		defer resp.Body.Close()
		_, err = io.Copy(w, resp.Body)
		if err != nil {
			handleError(w, url, err)
			return
		}
		log.Printf("Successfully read request from %s\n", url)
	}
}

func handleError(w http.ResponseWriter, url string, err error) {
	log.Printf("Error reading from %s - %v\n", url, err)
	io.WriteString(w, "can't read from upstream")
}
