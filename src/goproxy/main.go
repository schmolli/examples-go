package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/turngeek/examples-go/src/goproxy/handlers"
)

func main() {
	log.Println("Starting HTTP proxy server")
	router := mux.NewRouter()
	router.HandleFunc("/google{req:.*}", handlers.Proxy("https://www.google.com")).Methods("GET")
	router.HandleFunc("/fb{req:.*}", handlers.Proxy("https://www.facebook.com")).Methods("GET")
	if err := http.ListenAndServe(":8000", router); err != nil {
		log.Fatal(err)
	}
}
