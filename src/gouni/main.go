package main

import (
	"fmt"

	"github.com/turngeek/examples-go/src/gouni/model"
)

func main() {

	home := model.Address{}
	home.SetStreet("Burgstr. 39")
	home.SetCity("Reutlingen")

	s1 := model.CreateStudent("Schiesser", home)
	s1.AddGrade("ESD", 1.0)
	s1.AddGrade("BPT", 2.0)
	fmt.Printf("Student %s has an overall average grade of %.1f\n", s1.GetName(), s1.GetOverallAverageGrade())

	flat := model.CreateSharedFlat(model.CreateAddress("Pestalozzistr. 120", "Reutlingen"))
	flat.AddStudent(s1)
	fmt.Println(flat.String())
}
