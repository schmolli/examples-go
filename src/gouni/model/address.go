package model

type Address struct {
	street string
	city   string
}

func (adr *Address) String() string {
	return adr.street + ", " + adr.city
}

func (adr *Address) SetStreet(street string) {
	adr.street = street
}

func (adr *Address) GetStreet() string {
	return adr.street
}

func (adr *Address) SetCity(city string) {
	adr.city = city
}

func (adr *Address) GetCity() string {
	return adr.city
}

func CreateAddress(street string, city string) Address {
	return Address{street, city}
}
