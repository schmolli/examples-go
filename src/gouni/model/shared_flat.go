package model

import (
	"fmt"
	"strings"
)

type SharedFlat struct {
	address  Address
	students []Student
}

func (flat *SharedFlat) GetAddress() Address {
	return flat.address
}

func (flat *SharedFlat) SetAddress(address Address) {
	flat.address = address
}

func (flat *SharedFlat) AddStudent(student Student) {
	flat.students = append(flat.students, student)
}

func (flat *SharedFlat) GetNames() []string {
	names := []string{}
	for _, student := range flat.students {
		names = append(names, student.GetName())
	}
	return names
}

func (flat *SharedFlat) String() string {
	return fmt.Sprintf("Flat -> Address: %s, Students living in flat: %s", flat.address.String(), strings.Join(flat.GetNames(), ", "))
}

func CreateSharedFlat(address Address) SharedFlat {
	return SharedFlat{address, make([]Student, 0)}
}
