package model

type Student struct {
	name    string
	address Address
	grades  map[string]float32
}

func (student *Student) String() string {
	return student.name + " " + student.address.String()
}

func (student *Student) GetName() string {
	return student.name
}

func (student *Student) SetName(name string) {
	student.name = name
}

func (student *Student) GetAddress() Address {
	return student.address
}

func (student *Student) SetAddress(address Address) {
	student.address = address
}

func (student *Student) AddGrade(lecture string, grade float32) {
	student.grades[lecture] = grade
}

func (student *Student) GetOverallAverageGrade() float32 {
	var sum float32
	for _, grade := range student.grades {
		sum += grade
	}
	if sum == 0 {
		return 0.0
	} else {
		return sum / float32(len(student.grades))
	}
}

func CreateStudent(name string, address Address) Student {
	return Student{name, address, make(map[string]float32)}
}
