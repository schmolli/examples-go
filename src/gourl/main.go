package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	if len(os.Args) < 2 || len(os.Args) > 3 {
		fmt.Println("Usage: gourl [url]")
		return
	}
	url := os.Args[1]
	log.Printf("Calling %s ...", url)
	response, err := http.Get(url)
	if err != nil {
		log.Fatalf("Can't read from %s:\n#%v", url, err)
	}
	io.Copy(os.Stdout, response.Body)
	defer response.Body.Close()
}
