package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
)

const (
	url = "http://localhost:8080/hellowww"
)

type helloWWWRequest struct {
	Name string `json:"name"`
}

type helloWWWResponse struct {
	Message string `json:"message"`
}

func main() {
	log.Printf("Client is sending a request to %v\n", url)
	request := helloWWWRequest{Name: "Marcus"}
	data, err := json.Marshal(&request)
	if err != nil {
		log.Fatalf("Error while marshalling request: %v", err)
	}
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		log.Fatalf("Error while sending request %s: %v", url, err)
	}
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	var response helloWWWResponse
	err = decoder.Decode(&response)
	if err != nil {
		log.Fatalf("Error reading from %s\n%#v", url, err)
	}
	log.Printf("Message: %s", response.Message)
}
