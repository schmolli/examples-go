package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type helloWWWRequest struct {
	Name string `json:"name"`
}

type helloWWWResponse struct {
	Message string `json:"message"`
}

func main() {
	port := 8080
	r := mux.NewRouter()
	r.HandleFunc("/hellowww", helloWWWHandler).Methods("GET")
	r.HandleFunc("/hellowww", helloYouHandler).Methods("POST")
	log.Printf("Starting server on port %v\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), r))
}

func helloWWWHandler(w http.ResponseWriter, r *http.Request) {
	response := helloWWWResponse{Message: "Hello WWW"}
	encoder := json.NewEncoder(w)
	encoder.Encode(&response)
}

func helloYouHandler(w http.ResponseWriter, r *http.Request) {
	var request helloWWWRequest
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&request)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}
	response := helloWWWResponse{Message: "Hello " + request.Name}
	encoder := json.NewEncoder(w)
	encoder.Encode(&response)
}
